$(document).ready(function () {
  $(function () {
    $("#accordion")
      .accordion({
        header: "> div > h3",
        collapsible: true,
      })
      .sortable({
        axis: "y",
        handle: "h3",
        stop: function (event, ui) {
          ui.item.children("h3").triggerHandler("focusout");
          $(this).accordion("refresh");
        },
      });
  });

  $(".arrowup1").click(function (e) {
    var a = $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec1").html();

    $(".psec1").html($(".psec4").html());
    $(".psec4").html(b);

    e.preventDefault();
    e.stopPropagation();
  });

  $(".arrowup2").click(function (e) {
    var a = $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec2").html();

    $(".psec2").html($(".psec1").html());
    $(".psec1").html(b);

    e.preventDefault();
    e.stopPropagation();
  });

  $(".arrowup3").click(function (e) {
    var a = $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec3").html();

    $(".psec3").html($(".psec2").html());
    $(".psec2").html(b);

    e.preventDefault();
    e.stopPropagation();
  });

  $(".arrowup4").click(function (e) {
    var a = $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec4").html();

    $(".psec4").html($(".psec3").html());
    $(".psec3").html(b);

    e.preventDefault();
    e.stopPropagation();
  });

  $(".arrowdown1").click(function (e) {
    var a = $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec1").html();

    $(".psec1").html($(".psec2").html());
    $(".psec2").html(b);

    e.preventDefault();
    e.stopPropagation();
  });

  $(".arrowdown2").click(function (e) {
    var a = $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec2")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec2").html();

    $(".psec2").html($(".psec3").html());
    $(".psec3").html(b);

    e.preventDefault();
    e.stopPropagation();
  });

  $(".arrowdown3").click(function (e) {
    var a = $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec3")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec3").html();

    $(".psec3").html($(".psec4").html());
    $(".psec4").html(b);

    e.preventDefault();
    e.stopPropagation();
  });

  $(".arrowdown4").click(function (e) {
    var a = $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec4")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue;

    $(".headsec1")
      .contents()
      .filter(function () {
        return this.nodeType == 3;
      })[0].nodeValue = a;

    // Swap Content
    var b = $(".psec4").html();

    $(".psec4").html($(".psec1").html());
    $(".psec1").html(b);

    e.preventDefault();
    e.stopPropagation();
  });
});
