$(document).ready(function () {
  $(function () {
    $("#search").on('input', function () {
      var keyword = $("#search").val();
      // console.log(keyword);
      $.ajax({
        async: true,
        url: "/story8/data?q=" + keyword,
        success: function (result) {
          $("#buku").empty();
          var books = result.items;
          console.log(books);
          books.forEach(cetakBuku);
          function cetakBuku(buku,i){
              var judul = buku.volumeInfo.title;
              var gambar = buku.volumeInfo.imageLinks.smallThumbnail;
              var penulis = buku.volumeInfo.authors;
              var desc = buku.volumeInfo.description;
              // $("#buku").append ("<li>" + judul + "<br><img src=" +gambar+"></li>");
              $("#buku").append (`
              <tr>
              <th scope="row">`+ (i+1) + `</th>
              <td>`+judul+`</td>
              <td>`+desc+`</td>
              <td>`+penulis+`</td>
              <td> <img src=` +gambar+`></td>
              </tr>`
              );
          }
        },
      });
    });
  });
});
