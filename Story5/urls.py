from django.urls import path
from . import views

app_name = 'Story5'

urlpatterns = [
	path('', views.daftarmatkul , name = 'daftarmatkul'),
    path('addmatkul/', views.addMatkul, name = 'addmatkul'),
    path('detail/<str:nama>', views.detail, name = 'detail')
]