from django.shortcuts import render,redirect, resolve_url
from django.http import HttpResponse, HttpResponseRedirect
from .forms import MatkulForm
from .models import Matkul

# Create your views here.
def daftarmatkul(request):
    if request.method == 'POST':
        matkul = Matkul.objects.get(namaMatkul=request.POST['namaMatkul'])
        matkul.delete()
        return redirect(request.path)
    matkuls = Matkul.objects.all()
    return render (request, 'Story5/daftar_matkul.html', {'matkuls':matkuls})

def addMatkul(request):
    submitted = False
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        if form.is_valid():
           form.save()
           return redirect('/story5/addmatkul/?submitted=True')
    else: 
        form = MatkulForm()
        if 'submitted' in request.GET:
            submitted=True

    
    content = {
        'form' : form,
        'submitted' : submitted
    }
    return render (request, 'Story5/add_matkul.html', content)

def detail(request, nama):
	matkul = Matkul.objects.get(namaMatkul=nama)
	content = {
		'matkul': matkul
	}
	return render(request, 'Story5/detail.html', content)