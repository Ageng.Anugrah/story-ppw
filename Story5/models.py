from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.
class Matkul(models.Model):
    namaMatkul = models.CharField('Course\'s Name ', max_length=100)
    dosenPengajar = models.CharField('Lecturer ', max_length=100)
    jumlahSKS = models.IntegerField('Credit(s) ')
    deskripsi = models.CharField('Description ', max_length=1000)
    term = models.CharField('Term ', max_length=100)
    kelas = models.CharField('Classroom ',max_length=100)

    def __str__(self):
	    return self.namaMatkul