from django.forms import ModelForm
from django import forms
from .models import Matkul

class MatkulForm(ModelForm):
    class Meta:
        model = Matkul
        fields = '__all__'
        help_texts ={
            'term' : 'example : Odd 2020/2021',
        }
        widgets ={
            'namaMatkul' : forms.TextInput(attrs = {
                                'placeholder' : "Input course's name"
                            }),
            'dosenPengajar' : forms.TextInput(attrs = {
                                'placeholder' : "Input lecturer's name"
                            }),
            'jumlahSKS' : forms.NumberInput(attrs = {
                                'placeholder' : "Input course's credit(s)"
                            }),
            'deskripsi' : forms.Textarea(attrs = {
                                'placeholder' : "Input course's description"
                            }),
            'term' : forms.TextInput(attrs = {
                                'placeholder' : "Input course's term"
                        }),
            'kelas' : forms.TextInput(attrs = {
                                'placeholder' : "Input course's classroom"
                        }),
        }