from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request, 'Story3/home.html')

def about(request):
	return render(request, 'Story3/about.html')

def edu(request):
	return render(request, 'Story3/education.html')