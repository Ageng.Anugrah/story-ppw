from django.urls import path
from . import views

app_name = 'Story3'

urlpatterns = [
	path('', views.index , name = 'home'),
	path('about/', views.about , name = 'about'),
	path('edu/', views.edu , name = 'education'),
]
