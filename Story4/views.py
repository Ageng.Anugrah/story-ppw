from django.shortcuts import render

# Create your views here.
def index(request):
	return render(request, 'Story4/home.html')

def gallery(request):
	return render(request, 'Story4/gallery.html')

