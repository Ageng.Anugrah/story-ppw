from django.urls import path
from . import views

app_name = 'Story4'

urlpatterns = [
	path('', views.index , name = 'story4home'),
	path('gallery/', views.gallery , name = 'gallery'),
]