from django.urls import path
from . import views

app_name = 'Story7'

urlpatterns = [
	path('', views.story7 , name = 'story7'),
]