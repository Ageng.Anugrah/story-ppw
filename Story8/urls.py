from django.urls import path
from . import views

app_name = 'Story8'

urlpatterns = [
	path('', views.story8 , name = 'story8'),
	path('data/', views.data , name = 'story8data'),
]