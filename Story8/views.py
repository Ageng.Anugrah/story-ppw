from django.shortcuts import render
import requests
from django.http import JsonResponse

# Create your views here.
def story8(request):
    return render(request, 'Story8/index.html')

def data(request):
    # print("adsada")
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET.get("q", "Taubat") + "&maxResults=20"
    resp = requests.get(url)
    data = resp.json()
    return JsonResponse(data, safe=False)