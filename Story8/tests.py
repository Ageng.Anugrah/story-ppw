from django.test import TestCase, Client
from .views import story8, data
from django.urls import resolve

# Create your tests here.
class Story8test(TestCase):
    def test_landing_page_is_exist(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_func_story8(self):
        response = resolve('/story8/')
        self.assertEqual(response.func, story8)

    def test_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'Story8/index.html')
        self.assertTemplateUsed(response, 'base.html')

    def test_json_page_ada(self):
        response = Client().get('/story8/data/')
        self.assertEqual(response.status_code, 200)

    def test_cari_buku(self):
        response = Client().get('/story8/data?q=hai')
        # print(response)
        self.assertEqual(response.status_code, 301)

    def test_ada_search_box(self):
        response = Client().get('/story8/')
        self.assertContains(response, 'search')

    def test_table(self):
        response = Client().get('/story8/')
        self.assertContains(response, '</th>')
        self.assertContains(response, 'Title</th>')
        self.assertContains(response, 'Description</th>')
        self.assertContains(response, 'Cover</th>')
        self.assertContains(response, 'Authors</th>')
