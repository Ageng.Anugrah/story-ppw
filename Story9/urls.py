from django.urls import path
from . import views

app_name = 'Story9'

urlpatterns = [
	path('', views.story9 , name = 'story9'),
	path('signup/', views.user_signup, name = 'signup'),
	path('login/', views.user_login, name = 'login'),
	path('logout/', views.user_logout, name = 'logout')
]