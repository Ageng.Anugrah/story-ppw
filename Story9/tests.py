from django.test import TestCase, Client
from django.contrib.auth.models import User

# Create your tests here.
class Story9test(TestCase):
    def test_halaman_utama(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_halaman_login(self):
        response = Client().get('/story9/login/')
        self.assertEqual(response.status_code, 200)

    def test_halaman_signup(self):
        response = Client().get('/story9/signup/')
        self.assertEqual(response.status_code, 200)

    def test_halaman_logout(self):
        response = Client().get('/story9/logout/')
        self.assertEqual(response.status_code, 302)

    def test_url_ada_login(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/story9/login/')
        self.assertEqual(response.status_code, 302)

    def test_url_ada_signup(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        login = c.login(username=new_account.username, password='test123')
        response = c.get('/story9/signup/')
        self.assertEqual(response.status_code, 302)

    def test_save_signin_post(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        # login = c.login(username=new_account.username, password='test123')
        response = c.post(
                '/story9/login/',
                data = {
                    'uname' : 'testing',
                    'password' : 'test123'
                }
            )
        self.assertEqual(response.status_code, 302)

    def test_save_signin_post_asal(self):
        new_account = User.objects.create_user('testing', '', 'test123')
        new_account.set_password('test123')
        new_account.save()
        c = Client()
        # login = c.login(username=new_account.username, password='test123')
        response = c.post(
                '/story9/login/',
                data = {
                    'uname' : 'testing55',
                    'password' : 'test123'
                }
            )
        self.assertEqual(response.status_code, 200)

    def test_save_signup_post(self):
        c = Client()
        # login = c.login(username=new_account.username, password='test123')
        response = c.post(
                '/story9/signup/',
                data = {
                    'uname' : 'testing',
                    'password' : 'test123'
                }
            )
        self.assertEqual(1, User.objects.all().count())
        self.assertEqual(response.status_code, 302)