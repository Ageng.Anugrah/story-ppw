from django.urls import path
from . import views

app_name = 'Story1'

urlpatterns = [
	path('', views.index , name = 'index'),
	path('story1/', views.index2 , name = 'story1')
]
