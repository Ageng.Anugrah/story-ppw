from django.test import TestCase, Client
from django.urls import resolve, reverse
from django.shortcuts import render, redirect, resolve_url
from .views import daftaracara, addevent, addparticipant, deleteevent, removeparticipant
from .models import Acara, Peserta

# Create your tests here.


class Story6test(TestCase):

    # URL
    def test_url_ada(self):
        response = Client().get('/story6/')
        self.assertEqual(response.status_code, 200)

    def test_make_fungsi_daftaracara(self):
        found = resolve('/story6/')
        self.assertEqual(found.func, daftaracara)

    def test_redirect_habis_nambahin_event(self):
        response = Client().get('/story6/addevent')
        self.assertEqual(response.status_code, 302)

    def test_redirect_habis_nambahin_peserta(self):
        response = Client().get('/story6/addparticipant')
        self.assertEqual(response.status_code, 302)

    # Model
    def test_acara_create(self):
        acara = Acara.objects.create(namaacara='Tidur')
        count = Acara.objects.all().count()
        self.assertEqual(str(acara), acara.namaacara)
        self.assertEqual(count, 1)

    def test_peserta_create(self):
        acara = Acara.objects.create(namaacara='Tidur')
        peserta = Peserta.objects.create(nama="Ageng", acara=acara)
        count = Peserta.objects.all().count()
        self.assertEqual(str(peserta), peserta.nama)
        self.assertEqual(count, 1)

    def test_acara_hapus(self):
        acara = Acara.objects.create(namaacara='Tidur')
        acara.delete()
        count = Acara.objects.all().count()
        self.assertEqual(count, 0)

    def test_peserta_hapus(self):
        acara = Acara.objects.create(namaacara='Tidur')
        peserta = Peserta.objects.create(nama="Ageng", acara=acara)
        peserta.delete()
        count = Peserta.objects.all().count()
        self.assertEqual(count, 0)

    def test_hapus_keduanya(self):
        acara = Acara.objects.create(namaacara='Tidur')
        peserta = Peserta.objects.create(nama="Ageng", acara=acara)
        acara.delete()
        count1 = Acara.objects.all().count()
        count2 = Peserta.objects.all().count()
        self.assertEqual(count1, 0)
        self.assertEqual(count2, 0)

    def test_delete_from_get(self):
        response = Client().get('/story6/deleteevent/0',)
        self.assertEqual(response.status_code, 404)


    # Views
    def test_save_event_post(self):
        response = Client().post('/story6/addevent',data={'namaacara': 'Tidur'})
        count = Acara.objects.all().count()
        self.assertEqual(count, 1)
        self.assertEqual(response.status_code, 302)

    def test_save_peserta_post(self):
        acara = Acara.objects.create(namaacara='Tidur')
        response = Client().post('/story6/addparticipant',data={'nama':'Ageng', 'acara':acara.id})
        count = Acara.objects.all().count()
        self.assertEqual(count, 1)
        count1 = Peserta.objects.all().count()
        self.assertEqual(count1, 1)
        self.assertEqual(response.status_code, 302)

    def test_delete_event_post(self):
        acara = Acara.objects.create(namaacara='Tidur')
        response = Client().post('/story6/deleteevent',data={'event_id': acara.id})
        count = Acara.objects.all().count()
        self.assertEqual(count, 0)
        self.assertEqual(response.status_code, 302)

    def test_remove_peserta_post(self):
        acara = Acara.objects.create(namaacara='Tidur')
        peserta = Peserta.objects.create(nama="Ageng", acara=acara)
        response = Client().post('/story6/removeparticipant',data={'peserta_id': peserta.id})
        count = Peserta.objects.all().count()
        self.assertEqual(count, 0)
        self.assertEqual(response.status_code, 302)