from django.contrib import admin
from .models import Acara, Peserta

# Register your models here.
admin.site.register(Acara)
admin.site.register(Peserta)