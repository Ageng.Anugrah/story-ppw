from django.urls import path
from . import views

app_name = 'Story6'

urlpatterns = [
	path('', views.daftaracara , name = 'daftaracara'),
	path('addevent', views.addevent , name = 'addevent'),
	path('deleteevent', views.deleteevent , name = 'deleteevent'),
	path('addparticipant', views.addparticipant , name = 'addparticipant'),
	path('removeparticipant', views.removeparticipant , name = 'removeparticipant')
]