from django.db import models

# Create your models here.
class Acara(models.Model):
    namaacara = models.CharField('Nama ', max_length=100)
    
    def __str__(self):
        return self.namaacara

class Peserta(models.Model):
    acara = models.ForeignKey(Acara, on_delete = models.CASCADE)
    nama = models.CharField('Nama ', max_length=100)

    def __str__(self):
        return self.nama 