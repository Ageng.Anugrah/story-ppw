from django.shortcuts import render, redirect, resolve_url
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .forms import AcaraForm, PesertaForm
from .models import Acara, Peserta

# Create your views here.


def daftaracara(request):
    formAcara = AcaraForm()
    formPeserta = PesertaForm()
    acaras = Acara.objects.all()
    pesertas = Peserta.objects.all()
    acarapeserta = [(i, Peserta.objects.filter(acara=i)) for i in acaras]
    content = {
        'acaraform': formAcara,
        'pesertaform': formPeserta,
        'acarapeserta': acarapeserta
    }
    return render(request, 'Story6/index.html', content)

def addevent(request):
    form = AcaraForm(request.POST or None)
    if (request.method=="POST" and form.is_valid):
        form.save()
    return redirect(reverse('Story6:daftaracara'))

def addparticipant(request):
    form = PesertaForm(request.POST or None)
    if (request.method=="POST" and form.is_valid):
        form.save()
    return redirect(reverse('Story6:daftaracara'))

def deleteevent(request):
    acaraid = request.POST.get('event_id',None)
    acara = Acara.objects.get(id=acaraid)
    acara.delete()
    return redirect(reverse('Story6:daftaracara'))

def removeparticipant(request):
    participantid = request.POST.get('peserta_id',None)
    participant = Peserta.objects.get(id=participantid)
    participant.delete()
    return redirect(reverse('Story6:daftaracara'))