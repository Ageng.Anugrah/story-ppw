from django.forms import ModelForm
from django import forms
from .models import Acara, Peserta


class AcaraForm(ModelForm):
    class Meta:
        model = Acara
        fields = '__all__'
        widgets = {
            'namaacara': forms.TextInput(attrs={
                'placeholder': "Input your event",
            }),
        }


class PesertaForm(ModelForm):
    class Meta:
        model = Peserta
        fields = '__all__'
        widgets = {
            'nama': forms.TextInput(attrs={
                'placeholder': "Input name here",
            }),
        }
